# Copyright (c) 2023 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of GitLab Account Approval Bot.
#
# GitLab Account Approval Bot is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# GitLab Account Approval Bot is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# GitLab Account Approval Bot.  If not, see <http://www.gnu.org/licenses/>.
import datetime
import logging

import mwclient

from . import settings

logger = logging.getLogger(__name__)


class Client:
    """MediaWiki client."""

    _default_instance = None

    @classmethod
    def default_client(cls):
        """Get a MediaWiki client using the default credentials."""
        if cls._default_instance is None:
            logger.debug("Creating default instance")
            cls._default_instance = cls(
                settings.MEDIAWIKI_HOST,
                settings.MEDIAWIKI_CONSUMER_TOKEN,
                settings.MEDIAWIKI_CONSUMER_SECRET,
                settings.MEDIAWIKI_ACCESS_TOKEN,
                settings.MEDIAWIKI_ACCESS_SECRET,
                settings.MEDIAWIKI_LOG_PAGE,
            )
        return cls._default_instance

    def __init__(
        self,
        host,
        consumer_token,
        consumer_secret,
        access_token,
        access_secret,
        log_page,
    ):
        self.mwsite = mwclient.Site(
            host,
            consumer_token=consumer_token,
            consumer_secret=consumer_secret,
            access_token=access_token,
            access_secret=access_secret,
            clients_useragent="glaab (https://wikitech.wikimedia.org/wiki/Tool:Gitlab-account-approval)",
        )
        self.log_page = log_page

    def _log_action(self, log, summary):
        """Log an action on-wiki."""
        now = datetime.datetime.now()
        target_section = now.strftime("=== %Y-%m-%d ===")

        logline = f"* {now.hour:02d}:{now.minute:02d} {log}"

        page = self.mwsite.Pages[self.log_page]
        text = page.text()
        lines = text.split("\n")
        first_header = 0

        for pos, line in enumerate(lines):
            if line.startswith("=== "):
                first_header = pos
                break
        if lines[first_header] == target_section:
            lines.insert(first_header + 1, logline)
        else:
            lines.insert(first_header, "")
            lines.insert(first_header, logline)
            lines.insert(first_header, target_section)

        page.save("\n".join(lines), summary=summary, bot=True)

    def log_account_approval(self, gitlab_user):
        """Log an account approval action on-wiki."""
        username = gitlab_user["username"]
        log = f"[[gitlab:{username}|@{username}]] was approved."
        summary = f"@{username} was approved."
        return self._log_action(log, summary)

    def log_account_rejection(self, gitlab_user):
        """Log an account rejection action on-wiki."""
        username = gitlab_user["username"]
        created = gitlab_user["created_at"]
        log = f'"{username}" was rejected ' f"(pending since {created})."
        summary = f"{username} was rejected."
        return self._log_action(log, summary)
