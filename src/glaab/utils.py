# Copyright (c) 2023 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of GitLab Account Approval Bot.
#
# GitLab Account Approval Bot is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# GitLab Account Approval Bot is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# GitLab Account Approval Bot.  If not, see <http://www.gnu.org/licenses/>.
import datetime
import functools
import logging
import sys

from . import gerrit
from . import gitlab
from . import ldap
from . import phabricator
from . import settings

logger = logging.getLogger(__name__)
unhandled_logger = logging.getLogger("glaab.unhandled")
gerrit_client = gerrit.RESTClient.default_client()
gitlab_client = gitlab.Client.default_client()
ldap_client = ldap.Client.default_client()
phab_client = phabricator.Client.default_client()


def is_trusted(gitlab_user):
    """Should the given user be considered trusted?"""
    logger.info("Checking %s", gitlab_user["username"])

    ldap_user = get_developer(gitlab_user)
    if is_trusted_developer(ldap_user):
        return True

    if is_trusted_gerrit_user(ldap_user):
        return True

    phab_user = get_phabricator_user(ldap_user)
    if phab_user is not None and is_trusted_phab_user(phab_user):
        return True

    return False


def get_developer(gitlab_user):
    """Get the Developer account record for a gitlab user."""
    cn = None
    for ident in gitlab_user["identities"]:
        if ident["provider"] == "openid_connect":
            cn = ident["extern_uid"]
    return ldap_client.developer(cn)["attributes"]


def is_trusted_developer(ldap_user):
    """Is the given Developer account trusted by the community?"""
    groups = ldap_user.get("memberOf")
    uid = ldap_user["uid"][0]
    if not groups:
        logger.debug("%s is not a member of any groups", uid)
        return False
    for group in settings.LDAP_TRUSTED_GROUPS:
        if group in ldap_user.get("memberOf"):
            logger.info("%s is a memberOf %s", uid, group)
            return True
    return False


def get_phabricator_user(ldap_user):
    """Get the Phabricator account for a Developer account record."""
    users = phab_client.user_external_lookup(
        ldap_user["cn"],
        [ldap_user.get("wikimediaGlobalAccountName")],
    )
    if users:
        return users[0]
    return None


@functools.cache
def trusted_phab_user_phids():
    """Get a list of trusted Phabricator user PHIDs."""
    return phab_client.project_members(
        settings.PHABRICATOR_TRUSTED_GROUP,
    )


def is_trusted_phab_user(phab_user):
    """Is the given Phabricator user trusted by the community?"""
    if phab_user["phid"] in trusted_phab_user_phids():
        logger.info("%s is a trusted Phabricator user", phab_user["userName"])
        return True
    return False


@functools.cache
def trusted_gerrit_users():
    """Get a dict of Developer accounts trusted by Gerrit."""
    return gerrit_client.all_members(settings.GERRIT_TRUSTED_GROUP)


def is_trusted_gerrit_user(ldap_user):
    """Is the given Developer account trusted by Gerrit?"""
    if ldap_user["uid"][0] in trusted_gerrit_users():
        logger.info("%s is a trusted Gerrit user", ldap_user["uid"])
        return True
    return False


def is_expired(gitlab_user, days):
    """Is the given GitLab user more than N days old?"""
    now = datetime.datetime.now()
    created_at = datetime.datetime.strptime(
        gitlab_user["created_at"],
        "%Y-%m-%dT%H:%M:%S.%fZ",
    )
    elapsed = now - created_at
    if elapsed.days > days:
        logger.info(
            "%s was more than %s days ago.",
            gitlab_user["created_at"],
            days,
        )
        return True
    return False


def log_uncaught_exception(exc_type, exc_value, exc_traceback):
    """`sys.excepthook` handler that logs via the logging module."""
    if issubclass(exc_type, KeyboardInterrupt):
        # Ignore ^C exits
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    unhandled_logger.critical(
        "Uncaught exception",
        exc_info=(exc_type, exc_value, exc_traceback),
    )
